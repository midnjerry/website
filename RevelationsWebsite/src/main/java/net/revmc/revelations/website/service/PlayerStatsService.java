package net.revmc.revelations.website.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.revmc.revelations.website.domain.PlayerStats;
import net.revmc.revelations.website.repository.PlayerStatsRepository;

@Service
public class PlayerStatsService {
	@Autowired
	private PlayerStatsRepository playerStatsRepository;

	public List<PlayerStats> getAll() {
		return playerStatsRepository.findAll();
	}

	public PlayerStats getPlayer(int ID) {
		return playerStatsRepository.findOne(ID);
	}
}
