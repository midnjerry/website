package net.revmc.revelations.website.domain;

import java.util.UUID;

public class Entry {
	private String name;
	private String type;
	private UUID uuid;
	private String address;
	private String serviceName;
	private Long timestamp;
	private int points;
	private String description;

	public Entry(String name, String type, UUID uuid, String address, String serviceName, Long timestamp, int points,
			String description) {
		super();
		this.name = name;
		this.type = type;
		this.uuid = uuid;
		this.address = address;
		this.serviceName = serviceName;
		this.timestamp = timestamp;
		this.points = points;
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public String getType() {
		return type;
	}

	public UUID getUuid() {
		return uuid;
	}

	public String getAddress() {
		return address;
	}

	public String getServiceName() {
		return serviceName;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public int getPoints() {
		return points;
	}

	public String getDescription() {
		return description;
	}

	@Override
	public String toString() {
		return "Entry [name=" + name + ", serviceName=" + serviceName + ", timestamp=" + timestamp + ", points="
				+ points + "]";
	}
}
