package net.revmc.revelations.website.domain;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;
import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;

@Entity
@Table(name = "Stats")
@JsonInclude(NON_NULL)
public class PlayerStats implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 9099268155279550088L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	private int Id;

	@Column
	private String Name;
	@Column
	private String Gametype;
	@Column
	private double Kills;
	@Column
	private double Deaths;
	@Column
	private double Rating;
	@Column
	private double Souls;
	@Column
	private double Wins;
	@Column
	private double Losses;
	@Column
	private double Draws;
	@Column
	private double Points;

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getGametype() {
		return Gametype;
	}

	public void setGametype(String gameType) {
		Gametype = gameType;
	}

	public double getKills() {
		return Kills;
	}

	public void setKills(double kills) {
		Kills = kills;
	}

	public double getDeaths() {
		return Deaths;
	}

	public void setDeaths(double deaths) {
		Deaths = deaths;
	}

	public double getRating() {
		return Rating;
	}

	public void setRating(double rating) {
		Rating = rating;
	}

	public double getSouls() {
		return Souls;
	}

	public void setSouls(double souls) {
		Souls = souls;
	}

	public double getWins() {
		return Wins;
	}

	public void setWins(double wins) {
		Wins = wins;
	}

	public double getLosses() {
		return Losses;
	}

	public void setLosses(double losses) {
		Losses = losses;
	}

	public double getDraws() {
		return Draws;
	}

	public void setDraws(double draws) {
		Draws = draws;
	}

	public double getPoints() {
		return Points;
	}

	public void setPoints(double points) {
		Points = points;
	}

	@Column
	private String UUID_Text;

	public String getUUID_Text() {
		return UUID_Text;
	}

	public void setUUID_Text(String uUID_Text) {
		UUID_Text = uUID_Text;
	}
}
