package net.revmc.revelations.website.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.revmc.revelations.website.domain.PlayerStats;
import net.revmc.revelations.website.service.PlayerStatsService;

@RestController
@RequestMapping("/playerStats")
public class PlayerStatsResource {

    @Autowired
    private PlayerStatsService playerStatsService;

    @GetMapping
    public ResponseEntity<List<PlayerStats>> listOrders() {
		return ResponseEntity.ok(playerStatsService.getAll());
    }

	@GetMapping(value = "/{Id}")
	public ResponseEntity<PlayerStats> getOrder(@PathVariable int Id) {
		PlayerStats order = playerStatsService.getPlayer(Id);
        if (order == null) {
			return ResponseEntity.notFound().build();
        }
		return ResponseEntity.ok(order);
    }
}