package net.revmc.revelations.website.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.revmc.revelations.website.domain.FactionStats;
import net.revmc.revelations.website.repository.FactionStatsRepository;

@Service
public class FactionStatsService {
	@Autowired
	private FactionStatsRepository factionStatsRepository;

	public List<FactionStats> getAll() {
		return factionStatsRepository.findAll();
	}

	public FactionStats getFaction(int ID) {
		return factionStatsRepository.findOne(ID);
	}
}
