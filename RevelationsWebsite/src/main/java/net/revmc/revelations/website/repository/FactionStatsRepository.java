package net.revmc.revelations.website.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.revmc.revelations.website.domain.FactionStats;

@Repository
public interface FactionStatsRepository extends JpaRepository<FactionStats, Serializable> {

}
