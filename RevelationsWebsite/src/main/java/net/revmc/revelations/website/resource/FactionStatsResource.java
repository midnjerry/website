package net.revmc.revelations.website.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.revmc.revelations.website.domain.FactionStats;
import net.revmc.revelations.website.service.FactionStatsService;

@RestController
@RequestMapping("/factionStats")
public class FactionStatsResource {

    @Autowired
	private FactionStatsService factionStatsService;

    @GetMapping
	public ResponseEntity<List<FactionStats>> listOrders() {
		return ResponseEntity.ok(factionStatsService.getAll());
    }

	@GetMapping(value = "/{Id}")
	public ResponseEntity<FactionStats> getOrder(@PathVariable int Id) {
		FactionStats faction = factionStatsService.getFaction(Id);
		if (faction == null) {
			return ResponseEntity.notFound().build();
        }
		return ResponseEntity.ok(faction);
    }
}